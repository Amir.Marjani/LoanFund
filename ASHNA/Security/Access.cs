﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using ASHNA.Models;
using ACoder;
using Newtonsoft.Json;
using System.Configuration;
using ASHNA.Controllers;
using System.Web.Routing;

namespace ASHNA.Security
{
    public class AccessCheckAttribute : AuthorizeAttribute
    {
        ASHNAEntities db = new ASHNAEntities();

        public string UsersConfigKey { get; set; }
        public string RolesConfigKey { get; set; }

        protected virtual Principal CurrentUser
        {
            get { return HttpContext.Current.User as Principal; }
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {

            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                var Roles = SystemRolesConfig.GetRoles(filterContext.HttpContext.Request.RawUrl);
                if (CurrentUser.User.Lock)
                {
                    filterContext.HttpContext.Response.Redirect("/Default/Lock");
                }
                if (Roles.Length != 0)
                {
                    if (!CurrentUser.IsInRole(Roles))
                    {
                        filterContext.HttpContext.Response.Redirect("/SysError/UnAuthorized");
                    }
                }
            }
        }
    }
}