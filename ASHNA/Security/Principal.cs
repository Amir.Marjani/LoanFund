﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using ASHNA.Models;
namespace ASHNA.Security
{
    public class Principal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            string[] _roles = role.Split(',');
            if (_roles.Any(r => User.roles.ToLower().Contains(r.ToLower()))) return true;
            else return false;
        }
        public Principal(string Username, PrincipalSerializeModel _User)
        {
            Identity = new GenericIdentity(Username);
            User = _User;
        }

        public PrincipalSerializeModel User { get; private set; }
    }

    public class PrincipalSerializeModel : User
    {
        public string roles { get; set; }
        public bool Lock { get; set; }
    }

}