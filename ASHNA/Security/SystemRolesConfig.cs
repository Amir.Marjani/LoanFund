﻿using ASHNA.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ASHNA.Security
{
    public static class SystemRolesConfig
    {
        private static JObject ReadRolesFile()
        {
            return JObject.Parse(File.ReadAllText(HttpContext.Current.Request.PhysicalApplicationPath + "/Security/RolesConfig.json"));
        }
        public static string GetRoles(string path)
        {
            JObject Config = ReadRolesFile();
            string[] parts = path.ToLower().Split('/');
            if (parts.Length < 4)
            {
                Array.Resize<string>(ref parts, parts.Length + 1);
                parts[parts.Length - 1] = "index";
            }
            string roles = "";
            foreach (var j in Config[parts[1]][parts[2]][parts[3]])
            {
                roles += j.ToString() + ",";
            }
            return roles.Substring(0, roles.Length - 1);
        }
    }
}