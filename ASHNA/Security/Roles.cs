﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASHNA.Models;
namespace ASHNA.Security
{
    public static class Roles
    {
        static ASHNAEntities db = new ASHNAEntities();
        public static string GetRoles(User user)
        {
            User _user = db.Users.Find(user.ID);
            List<UsersGroup> usersgroups = _user.UsersGroups.Where(r => r.User.ID == user.ID).ToList();
            List<Group> groups = new List<Group>();

            string roles = "";
            foreach (UsersGroup ug in usersgroups)
            {
                groups.Add(ug.Group);
            }
            foreach (Group g in groups)
            {
                foreach (GroupsRole gr in g.GroupsRoles)
                {
                    roles += gr.Role.Value + ",";
                }
            }
            return roles.Substring(0, roles.Length - 1);
        }
    }
}