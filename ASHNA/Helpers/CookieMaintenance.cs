﻿using ASHNA.Models;
using ASHNA.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace ASHNA.Helpers
{
    public static class CookieMaintenance
    {
        static ASHNAEntities db = new ASHNAEntities();

        public static bool CheckCookie(HttpRequest Request, string Name)
        {
            HttpCookie cookie = Request.Cookies[Name];
            if (cookie == null || cookie.Value == null || cookie.Value.Length == 0)
                return false;
            else return true;
        }
        public static bool CheckCookie(HttpRequestBase Request, string Name)
        {
            HttpCookie cookie = Request.Cookies[Name];
            if (cookie == null || cookie.Value == null || cookie.Value.Length == 0)
                return false;
            else return true;
        }
        public static PrincipalSerializeModel GetUsersCookie(HttpRequestBase Request)
        {
            FormsAuthenticationTicket lastAuthTicket = FormsAuthentication.Decrypt(Request.Cookies["Users"].Value);
            PrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<PrincipalSerializeModel>(lastAuthTicket.UserData);
            return serializeModel;
        }
        public static PrincipalSerializeModel GetUsersCookie(HttpRequest Request)
        {
            FormsAuthenticationTicket lastAuthTicket = FormsAuthentication.Decrypt(Request.Cookies["Users"].Value);
            PrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<PrincipalSerializeModel>(lastAuthTicket.UserData);
            return serializeModel;
        }
        public static HttpCookie SetUsersCookie(PrincipalSerializeModel Data)
        {
            string userData = JsonConvert.SerializeObject(Data);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, Data.Username, DateTime.Now, DateTime.Now.AddYears(99), false, userData);
            //string encTicket = cr.Encode(FormsAuthentication.Encrypt(authTicket), ConfigurationManager.AppSettings["cookiePass"]);
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            //  Session.Add("Users", encTicket);
            HttpCookie faCookie = new HttpCookie("Users");
            faCookie.Value = encTicket;
            faCookie.Expires = DateTime.Now.AddYears(99);
            return faCookie;
        }
        public static HttpCookie UpdateUsersCookie(HttpRequestBase Request, PrincipalSerializeModel Data, bool SetLock)
        {
            PrincipalSerializeModel serializeModel = GetUsersCookie(Request);
            serializeModel.Username = (Data.Username != null) ? Data.Username : serializeModel.Username;
            serializeModel.FirstName = (Data.FirstName != null) ? Data.FirstName : serializeModel.FirstName;
            serializeModel.LastName = (Data.LastName != null) ? Data.LastName : serializeModel.LastName;
            serializeModel.Email = (Data.Email != null) ? Data.Email : serializeModel.Email;
            serializeModel.Phone = (Data.Phone != null) ? Data.Phone : serializeModel.Phone;
            serializeModel.roles = (Data.roles != null) ? Data.roles : serializeModel.roles;
            serializeModel.Lock = (SetLock) ? Data.Lock : serializeModel.Lock;
            return SetUsersCookie(serializeModel);
        }
        public static HttpCookie CreateCookie(string Name, int? Expire)
        {
            HttpCookie Cookie = new HttpCookie(Name);
            Cookie.Expires = DateTime.Now.AddMinutes(Convert.ToInt16(Expire));
            Cookie.Value = Name;
            return Cookie;
        }
    }
}