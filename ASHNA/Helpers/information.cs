﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASHNA.Helpers
{
    public class information
    {
        public int personalId { get; set; }
        public string name { get; set; }
        public string family { get; set; }
        public string fatherName { get; set; }
        public int nationalCode { get; set; }
        public int birthdayDate { get; set; }
        public int membershipDate { get; set; }
        public string phone { get; set; }
        public string cellPhone { get; set; }
        public string address { get; set; }
    }
}