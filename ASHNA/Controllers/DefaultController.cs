﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASHNA.Models;
using Newtonsoft.Json;
using ACoder;
using System.Web.Mvc.Filters;
using ASHNA.Security;
using System.Web.Security;
using System.Configuration;
using ASHNA.Helpers;
namespace ASHNA.Controllers
{
    [AllowAnonymous]
    public class DefaultController : Controller
    {
        ASHNAEntities db = new ASHNAEntities();
        AuthenticationContext aut = new AuthenticationContext();
        // GET: Default

        public ActionResult Login(string Url = null)
        {
            ViewBag.title = "Login";
            ViewBag.url = Url;

            return View();
        }
        [HttpPost]
        //public ActionResult Login(string Username, string Password, string Url = null)
        //{
        //    ViewBag.title = "Login";
        //    ViewBag.url = Url;
        //    try
        //    {
        //        User user = db.USP_Authenticate(Username, Password).First();
        //        string roles = Security.Roles.GetRoles(user);
        //        PrincipalSerializeModel serializeModel = new PrincipalSerializeModel();
        //        serializeModel.ID = user.ID;
        //        serializeModel.Username = user.Username;
        //        serializeModel.FirstName = user.FirstName;
        //        serializeModel.LastName = user.LastName;
        //        serializeModel.Email = user.Email;
        //        serializeModel.Phone = user.Phone;
        //        serializeModel.roles = roles;
        //        serializeModel.Lock = false;

        //        Response.Cookies.Add(CookieMaintenance.SetUsersCookie(serializeModel));
        //        Response.Cookies.Add(CookieMaintenance.CreateCookie("Lock", db.Settings.First().UserLoginTimeOut));

        //        //  Response.Redirect("/Dashboard/Main/Index", false);
        //        // return RedirectToAction("Index", "Main", new { area = "Dashboard" });
        //        if (Url != null && Url != "")
        //        {
        //            Response.Redirect(Url);
        //        }
        //        else
        //        {
        //            Response.Redirect("/Dashboard/Main");
        //        }
        //    }
        //    catch (Exception )
        //    {
        //        ViewBag.Error = true;
        //        return View();
        //    }
        //    return View();

        //}
        public ActionResult Register()
        {
            ViewBag.title = "Register";
            return View();
        }
        [HttpPost]
        //public ActionResult Register(string FirstName, string LastName, int Username, string Password, string RePassword, string Email, string Phone)
        //{
        //    ViewBag.title = "Register";
        //    try
        //    {
        //        if (Password == RePassword )
        //        {
        //            User user = db.USP_AddUser(Username, Password, FirstName, LastName, Email, Phone,"","","", "","","").First();
        //            UsersGroup usergroup = new UsersGroup();
        //            usergroup.GroupId_FK = db.Settings.First().DefaultUsersGroup;
        //            usergroup.UserId_FK = user.ID;
        //            db.UsersGroups.Add(usergroup);
        //            db.SaveChanges();
        //            ViewBag.Result = "success";
        //            return View();
        //        }
        //        else
        //        {
        //            ViewBag.Result = "error";
        //            return View();
        //        }
        //    }
        //    catch 
        //    {
        //        ViewBag.Result = "error";
        //        return View();
        //    }
        //}
        public ActionResult Lock(string Url = null)
        {
            ViewBag.url = Url;

            if (Request.Cookies["Users"] != null)
            {
                ViewBag.Error = false;
                PrincipalSerializeModel serializeModel = CookieMaintenance.GetUsersCookie(Request);
                serializeModel.Lock = true;
                Response.Cookies.Add(CookieMaintenance.UpdateUsersCookie(Request, serializeModel, true));
                return View(serializeModel);
            }
            return RedirectToAction("Login", "Default");
        }
        [HttpPost]
        //public ActionResult UnLock(string Username, string Password, string Url = null)
        //{
        //    ViewBag.url = Url;

        //    if (Request.Cookies["Users"] != null)
        //    {
        //        PrincipalSerializeModel serializeModel = CookieMaintenance.GetUsersCookie(Request);
        //        try
        //        {
        //            User user = db.USP_Authenticate(Username, Password).First();
        //            serializeModel.Lock = false;
        //            Response.Cookies.Add(CookieMaintenance.UpdateUsersCookie(Request, serializeModel, true));
        //            Response.Cookies.Add(CookieMaintenance.CreateCookie("Lock", db.Settings.First().UserLoginTimeOut));

        //            // return RedirectToAction("Index", "Main", new { area = "Dashboard" });
        //            if (Url != null && Url != "")
        //            {
        //                Response.Redirect(Url);
        //            }
        //            else
        //            {
        //                Response.Redirect("/Dashboard/Main");
        //            }
        //        }
        //        catch
        //        {
        //            ViewBag.Error = true;
        //            return View("Lock", serializeModel);
        //        }
        //    }
        //    else
        //        return RedirectToAction("Login", "Default");
        //    return RedirectToAction("Lock", "Default");
        //}
        [AllowAnonymous]
        public ActionResult SignOut()
        {

            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            Response.Cookies.Add(CookieMaintenance.CreateCookie("Lock", -1));

            return RedirectToAction("Login", "Default");
        }

    }
}