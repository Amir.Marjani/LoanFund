﻿using ACoder;
using ASHNA.Models;
using ASHNA.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ASHNA.Helpers;
namespace ASHNA.Controllers
{
    public class SysErrorController : Controller
    {
        ASHNAEntities db = new ASHNAEntities();
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UnAuthorized()
        {
            PrincipalSerializeModel user = CookieMaintenance.GetUsersCookie(Request);
            return View(user);
        }
    }
}