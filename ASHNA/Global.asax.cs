﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using ASHNA.Models;
using ASHNA.Security;
using Newtonsoft.Json;
using System.Configuration;
using ACoder;
using ASHNA.Helpers;
namespace ASHNA
{
    public class MvcApplication : System.Web.HttpApplication
    {
        ASHNAEntities db = new ASHNAEntities();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        public void Application_BeginRequest(object sender, EventArgs e)
        {
            bool _lock = CookieMaintenance.CheckCookie(Request, "Lock");
            bool _users = CookieMaintenance.CheckCookie(Request, "Users");
            //if (Request.Cookies["Users"] != null) _users = true; else _users = false;
            //if (Request.Cookies["Lock"] != null) _lock = true; else _lock = false;

            if (_users && !_lock)
            {
                if (Request.Path.ToLower().Contains("/dashboard"))
                    Response.Redirect("/Default/Lock/?Url=" + Request.RawUrl);
                //else
                //    Response.Redirect("/Default/Lock");
            }
            else if (_users && _lock)
            {
                if (Request.Path.ToLower().Contains("login"))
                    Response.Redirect("/Dashboard/Main");
                else
                {
                    Response.Cookies.Add(CookieMaintenance.CreateCookie("Lock", db.Settings.First().UserLoginTimeOut));
                }
            }
            else if (!_users && !_lock)
            {
                if (Request.Path.ToLower().Contains("/dashboard"))
                    Response.Redirect("/Default/Login/?Url=" + Request.RawUrl);
                //else
                //    Response.Redirect("/Default/Login");
            }
            else if (!_users && _lock)
            {
                Response.Cookies.Add(CookieMaintenance.CreateCookie("Lock", -1));
                Response.Redirect(HttpContext.Current.Request.RawUrl);
            }
        }
        void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            if (exc.GetType() == typeof(HttpException))
            {
                if (exc.Message.Contains("NoCatch") || exc.Message.Contains("maxUrlLength"))
                    return;
                Server.Transfer("/Errors/404.html");
            }
            Server.Transfer("/Errors/500.html");
            Server.ClearError();
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (CookieMaintenance.CheckCookie(Request, "Users"))
            {
                PrincipalSerializeModel serializeModel = CookieMaintenance.GetUsersCookie(Request);
                HttpContext.Current.User = new Principal(serializeModel.Username, serializeModel);
            }
        }
    }
}
