﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace ASHNA
{
    public class Process
    {
        PerformanceCounter cpuCounter, diskReadsPerformanceCounter, diskWritesPerformanceCounter, diskTransfersPerformanceCounter;
     
        public int CPU()
        {
            cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);
            cpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            return Convert.ToInt32(Math.Round(cpuCounter.NextValue()));
        }
        public int Memory()
        {

            Int64 phav = GetPhysicalAvailableMemoryInMiB();
            Int64 tot = GetTotalMemoryInMiB();
            decimal percentFree = ((decimal)phav / (decimal)tot) * 100;
            decimal percentOccupied = 100 - percentFree;
            return Convert.ToInt32(Math.Round(percentOccupied));
        }

        public int Disk()
        {

            int disk = 1;
            int free = 1;
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady)
                {
                    disk += Convert.ToInt32(((drive.TotalSize / 1024) / 1024) / 1024);
                    free += Convert.ToInt32(((drive.TotalFreeSpace / 1024) / 1024) / 1024);
                }

            }
            return Convert.ToInt32(((disk - free) * 100) / disk);
        }

        public string DiskReadWrite()
        {
            diskReadsPerformanceCounter = new PerformanceCounter("PhysicalDisk", "% Disk Read Time", "_Total");
            diskWritesPerformanceCounter = new PerformanceCounter("PhysicalDisk", "% Disk Write Time", "_Total");
            // diskTransfersPerformanceCounter = new PerformanceCounter("PhysicalDisk", "Disk Transfers/sec", "_Total");
            diskReadsPerformanceCounter.NextValue();
            diskWritesPerformanceCounter.NextValue();
            //  diskTransfersPerformanceCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            return Math.Round(diskReadsPerformanceCounter.NextValue()) + "-" + Math.Round(diskWritesPerformanceCounter.NextValue()); //+ "-" + diskTransfersPerformanceCounter.NextValue();
        }
        public string Network()
        {
            PerformanceCounterCategory category = new PerformanceCounterCategory("Network Interface");
            String[] instancename = category.GetInstanceNames();
            string data = "";
            //foreach (string name in instancename)
            //{
            //    data += getNetworkUtilization(name).ToString() + "-";
            //}


            return getNetworkUtilization(instancename[0]).ToString();

        }

        public string getNetworkUtilization(string networkCard)
        {

            const int numberOfIterations = 10;

            PerformanceCounter bandwidthCounter = new PerformanceCounter("Network Interface", "Current Bandwidth", networkCard);
            float bandwidth = bandwidthCounter.NextValue();//valor fixo 10Mb/100Mn/

            PerformanceCounter dataSentCounter = new PerformanceCounter("Network Interface", "Bytes Sent/sec", networkCard);

            PerformanceCounter dataReceivedCounter = new PerformanceCounter("Network Interface", "Bytes Received/sec", networkCard);

            float sendSum = 0;
            float receiveSum = 0;

            for (int index = 0; index < numberOfIterations; index++)
            {
                sendSum += dataSentCounter.NextValue();
                receiveSum += dataReceivedCounter.NextValue();
            }
            float dataSent = sendSum;
            float dataReceived = receiveSum;


            double receive = Convert.ToInt32(dataReceived / 1024);//((8 * (dataReceived)) / ((bandwidth) * numberOfIterations)) * 100;
            double send = Convert.ToInt32(dataSent / 1024);//((8 * (dataSent)) / ((bandwidth) * numberOfIterations)) * 100;

            return send.ToString() + "-" + receive.ToString() + "-" + (bandwidth / 1000).ToString();
        }

        [DllImport("psapi.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetPerformanceInfo([Out] out PerformanceInformation PerformanceInformation, [In] int Size);

        [StructLayout(LayoutKind.Sequential)]
        private struct PerformanceInformation
        {
            public int Size;
            public IntPtr CommitTotal;
            public IntPtr CommitLimit;
            public IntPtr CommitPeak;
            public IntPtr PhysicalTotal;
            public IntPtr PhysicalAvailable;
            public IntPtr SystemCache;
            public IntPtr KernelTotal;
            public IntPtr KernelPaged;
            public IntPtr KernelNonPaged;
            public IntPtr PageSize;
            public int HandlesCount;
            public int ProcessCount;
            public int ThreadCount;
        }

        private static Int64 GetPhysicalAvailableMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalAvailable.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }

        }

        private static Int64 GetTotalMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalTotal.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }

        }
    }
}