﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;

namespace ASHNA
{
    public class ChatHub : Hub
    {
        public void Send()
        {
            Process p = new Process();
            // Call the broadcastMessage method to update clients.

            // System.Threading.Thread.Sleep(1000);
            while (true)
            {
                System.Threading.Thread.Sleep(1500);
                Clients.All.receive(String.Format("{0}-{1}-{2}-{3}-{4}", p.CPU(), p.Disk(), p.Memory(), p.Network(), p.DiskReadWrite()));
            }

        }

    }
}