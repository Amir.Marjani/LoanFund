﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASHNA.Areas.Customers.Controllers
{
    public class ReportsController : Controller
    {
        public ActionResult ReportsDirection(string name, string not_found = "false")
        {
            ViewBag.PartialName = name;
            return View();
        }


        public ActionResult _PersonalReports()
        {
            return View();
        }


        public ActionResult _GroupsReports()
        {
            return View();

        }
        public ActionResult _FundFinancialBalance()
        {
            return View();
        }

        public ActionResult _FamilyMembershipFinancialbalance()
        {
            return View();
        }
    }
}