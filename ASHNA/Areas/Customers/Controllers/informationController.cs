﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASHNA.Models;

namespace ASHNA.Areas.Customers.Controllers
{
    public class informationController : Controller
    {
        ASHNAEntities db = new ASHNAEntities();
        // GET: information
        public ActionResult Addnew(string name, string not_found = "false")
        {
            ViewBag.error = not_found;
            ViewBag.PartialName = name;
            return View();
        }
        [HttpPost]
        public ActionResult Add(int PersonalCode, string PassWord, int Supervisor, string Name, string Family, int FirstUserAccount, string Phone, string CellPhone, string Email, DateTime BirthdayDate, DateTime MembershipDate, string Address, string FatherName, string NationalCode)
        {
            try
            {
                BirthdayDate = DateTime.Now;
                MembershipDate = DateTime.Now;

                db.SP_CustomersInformation(PersonalCode, PassWord, Supervisor,Name, Family, FirstUserAccount, Phone, CellPhone, Email, BirthdayDate, MembershipDate, Address, FatherName, NationalCode);
                return RedirectToAction("Addnew", "information", new {area = "Customers", name = "_addNew", not_found = "added" });
            }
            catch (Exception ex)
            {
                
                return RedirectToAction("Addnew", "information", new { area = "Customers", name = "_addNew", not_found = "Notadded" });
            }

        }



        public ActionResult Delete(string accountid)
        {
            User user = db.Users.Where(u => u.Username == accountid).FirstOrDefault();
            if (user == null)
            {
                return Redirect("/Customers/information/Addnew?name=_Delete&not_found=true");
            }
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Addnew", "information", new { area = "Customers", name = "_Delete", not_found = "deleted" });
        }




        public ActionResult Search(string accountid, string notification = "false")
        {
            ViewBag.success = notification;
            
            User user = db.Users.Where(u => u.Username == accountid).FirstOrDefault();
            if (user == null)
            {
                return Redirect("/Customers/information/Addnew?name=_Search&not_found=true");
            }
            return View(user);
        }



        [HttpPost]
        public ActionResult Update(string accountID, string name, string FamilyName, string FatherName, string NationalCode, string Phone, string CellPhone, string Address, string BirthdayDate, string MembershipDate)
        {
            User user = db.Users.Where(u => u.Username == accountID).FirstOrDefault();
            if (user == null)
            {
                return Redirect("/Customers/information/Addnew?name=_Search&not_found=true");
            }
            user.FirstName = name;
            user.LastName = FamilyName;
            user.FatherName = FatherName;
            user.NationalCode = NationalCode;
            user.BirthdayDate = BirthdayDate;
            user.MembershipDate = MembershipDate;
            user.Phone = Phone;
            user.CellPhone = CellPhone;
            user.Address = Address;
            db.SaveChanges();
            return RedirectToAction("Search", "information", new { area = "Customers", accountid = user.Username, notification = "true" });
        }
    }
}