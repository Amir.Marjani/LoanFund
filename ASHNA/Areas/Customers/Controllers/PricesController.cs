﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASHNA.Models;

namespace ASHNA.Areas.Customers.Controllers
{
    public class PricesController : Controller
    {
        // GET: Customers/PricesController
        ASHNAEntities db = new ASHNAEntities();
        // GET: Prices
        public ActionResult SingularCashAdd(string name, string not_found = "false")
        {
            ViewBag.PartialName = name;
            return View();
        }
        public JsonResult _AddNew()
        {
            List<string> countries = new List<string>();
            foreach (var m in db.Users.ToList())
            {
                countries.Add(m.Username + " " + m.FirstName + " " + m.LastName);
            }
            return Json(new { countries = countries }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult _Search()
        {

            return View();


        }

        public ActionResult _Delete()
        {

            return View();


        }
    }
}