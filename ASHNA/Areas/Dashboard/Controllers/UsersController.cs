﻿using ASHNA.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASHNA.Models;
using Newtonsoft.Json;

namespace ASHNA.Areas.Dashboard.Controllers
{
    public class UsersController : Controller
    {
        ASHNAEntities db = new ASHNAEntities();
        // GET: Dashboard/Users
        [AccessCheck]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [AccessCheck]
        public JsonResult GetUserInfo(int id)
        {
            User user = db.Users.Find(id);
            List<string> groups = new List<string>();
            foreach (var g in db.UsersGroups.Where(g => g.UserId_FK == id).ToList())
            {
                groups.Add(g.Group.Name);
            }
            return Json(new { username = user.Username, email = user.Email, fname = user.FirstName, lname = user.LastName, phone = user.Phone, groups = groups });
        }
        [HttpPost]
        [AccessCheck]
        public JsonResult GetGroupInfo(int id)
        {
            Group group = db.Groups.Find(id);
            List<string> roles = new List<string>();
            foreach (var g in db.GroupsRoles.Where(g => g.GroupId_FK == id).ToList())
            {
                roles.Add(g.Role.Name);
            }
            return Json(new { desc = group.Description, roles = roles });
        }

        [HttpPost]
        [AccessCheck]
        public JsonResult GetRoleInfo(int id)
        {
            Role role = db.Roles.Find(id);
            List<string> groups = new List<string>();
            foreach (var g in db.GroupsRoles.Where(g => g.GroupId_FK == id).ToList())
            {
                groups.Add(g.Group.Name);
            }
            return Json(new { value = role.Value, groups = groups });
        }
    }

}