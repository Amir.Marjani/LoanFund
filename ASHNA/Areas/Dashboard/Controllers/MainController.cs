﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using Owin;
using Microsoft.Owin;
using ACoder;
using ASHNA.Models;
using Newtonsoft.Json;
using ASHNA.Security;
using System.Web.Security;
using System.Configuration;

namespace ASHNA.Areas.Dashboard.Controllers
{
    public class MainController : Controller
    {
        ASHNAEntities db = new ASHNAEntities();
        // GET: Dashboard/Main
        [AccessCheck]
        public ActionResult Index()
        {
         //   var ticket = cr.Decode(Request.Cookies["Users"].Value, ConfigurationManager.AppSettings["cookiePass"]);
            string data = FormsAuthentication.Decrypt(Request.Cookies["Users"].Value).UserData.ToString();
            PrincipalSerializeModel user = JsonConvert.DeserializeObject<PrincipalSerializeModel>(data);

            return View();
        }
    }
}